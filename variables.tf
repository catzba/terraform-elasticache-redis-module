#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# VPC VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "subnet_private_id2" {
  type    = string
}

variable "subnet_private_id" {
  type    = string
}

#-----------------------------------------------------------------------------------------------------
# REDIS VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "instance_sg_id" {
  type = string
}

variable "redis_engine" {
  type = string
}

variable "redis_subnet_group" {
  type = string
}

variable "redis_cluster_id" {
  type = string
}

variable "redis_port" {
  type    = number
}

variable "redis_engine_version" {
  type = string
}

variable "redis_parameter_group_name" {
  type = string
}

variable "redis_num_cache_nodes" {
  type = number
}

variable "redis_node_type" {
  type = string
}

