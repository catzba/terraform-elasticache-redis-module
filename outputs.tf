output "redis_configuration_endpoint" {
  description = "Id of backend-sg"
  value       = aws_elasticache_cluster.redis.configuration_endpoint
}