#-----------------------------------------------------------------------------------------------------
# Elastic cache REDIS
#-----------------------------------------------------------------------------------------------------
resource "aws_elasticache_subnet_group" "redis" {
  name       = var.redis_subnet_group
  subnet_ids = [var.subnet_private_id, var.subnet_private_id2]
}

#-----------------------------------------------------------------------------------------------------
# Elastic cache REDIS
#-----------------------------------------------------------------------------------------------------
resource "aws_elasticache_cluster" "redis" {
  cluster_id           = var.redis_cluster_id
  engine               = var.redis_engine
  node_type            = var.redis_node_type
  num_cache_nodes      = var.redis_num_cache_nodes
  parameter_group_name = var.redis_parameter_group_name
  engine_version       = var.redis_engine_version
  port                 = var.redis_port
  subnet_group_name    = aws_elasticache_subnet_group.redis.name
  security_group_ids = [
    var.instance_sg_id
  ]

  tags = {
    Name        = "${var.project_name}-${var.environment}-redis"
    Project     = var.project_name
    Environment = var.environment
  }
}
